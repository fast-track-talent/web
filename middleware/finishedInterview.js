export default function ({ redirect }) {
  if (process.browser) {
    if (!localStorage.getItem('finished-assistant')) {
      redirect('/')
    }
  }
}
