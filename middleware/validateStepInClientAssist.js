export default function (context) {
  if (process.browser) {
    switch (context.route.name) {
      case 'profiles-contact':
        if (!localStorage.getItem('profilesIdentification'))
          context.redirect('/profiles/identification')
        break
      case 'profiles-industry-sector':
        if (!localStorage.getItem('profilesContact'))
          context.redirect('/profiles/contact')
        break
      case 'profiles-products':
        if (!localStorage.getItem('profilesIndustrySector'))
          context.redirect('/profiles/industry-sector')
        break
      case 'profiles-profile-requests':
        if (!localStorage.getItem('profilesProducts'))
          context.redirect('/profiles/products')
        break
      case 'profiles-profile-locations':
        if (!localStorage.getItem('profilesRequests'))
          context.redirect('/profiles/profile-requests')
        break
      case 'profiles-profile-studies':
        if (!localStorage.getItem('profilesLocations'))
          context.redirect('/profiles/profile-locations')
        break
      case 'profiles-profile-skills':
        if (!localStorage.getItem('profilesStudies'))
          context.redirect('/profiles/profile-studies')
        break
      case 'profiles-profile-years-of-experience':
        if (!localStorage.getItem('profilesSkills'))
          context.redirect('/profiles/profile-skills')
        break
      case 'profiles-confirm-information':
        if (!localStorage.getItem('profilesYearsOfExperience'))
          context.redirect('/profiles/profile-years-of-experience')
        break
    }
  }
}
