export default function (context) {
  if (process.browser) {
    switch (context.route.name) {
      case 'interview-personal-data':
        if (!localStorage.getItem('interviewContact'))
          context.redirect('/interview/contact')
        break
      case 'interview-salary-expectation':
        if (!localStorage.getItem('interviewPersonalData'))
          context.redirect('/interview/personal-data')
        break
      case 'interview-studies':
        if (!localStorage.getItem('interviewSalaryExpectation'))
          context.redirect('/interview/salary-expectation')
        break
      case 'interview-skills':
        if (!localStorage.getItem('interviewStudies'))
          context.redirect('/interview/studies')
        break
      case 'interview-work-experience':
        if (!localStorage.getItem('interviewSkills'))
          context.redirect('/interview/skills')
        break
      case 'interview-extra-data':
        if (!localStorage.getItem('interviewWorkExperience'))
          context.redirect('/interview/work-experience')
        break
      case 'interview-confirm-information':
        if (!localStorage.getItem('interviewExtraData'))
          context.redirect('/interview/extra-data')
        break
    }
  }
}
