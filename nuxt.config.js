export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Fast Track Talent',
    htmlAttrs: {
      lang: 'es',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      {
        name: 'facebook-domain-verification',
        content: 'k90xrdg5m7p4i2yaw4bjby42cb71jy',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: 'https://fonts.googleapis.com/icon?family=Material+Icons',
      },
    ],
  },
  googleAnalytics: {
    id: 'UA-198525481-2',
    debug: {
      sendHitTask: process.env.DEBUG_ANALYTICS === 'production',
    },
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/main.css', { src: '@/assets/css/index.scss', lang: 'scss' }],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-debounce',
    '~/plugins/vue-select',
    '~/plugins/vue-quill-editor',
    '~/plugins/vue-lodash.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // Google analytics
    '@nuxtjs/google-analytics',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
    // Google's Recaptcha.
    [
      '@nuxtjs/recaptcha',
      {
        hideBadge: true,
        siteKey: process.env.GOOGLE_RECAPTCHA_SITE_KEY,
        version: 3,
      },
    ],
    // Facebook pixel
    [
      'nuxt-facebook-pixel-module',
      {
        /* module options */
        track: 'PageView',
        pixelId: '1032519257621539',
        autoPageView: true,
        disabled: process.env.FP_DISABLED,
        debug: process.env.FP_DEBUG,
      },
    ],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.BASE_URL,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    loaders: {
      scss: {
        implementation: require('sass'),
      },
    },
  },
}
