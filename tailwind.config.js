module.exports = {
  purge: {
    mode: 'layers',
    enabled: 'development',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
      // TypeScript
      'plugins/**/*.ts',
      'nuxt.config.ts',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    // fontFamily: {
    //   sans: ['Montserrat'],
    //   serif: ['Montserrat'],
    //   mono: ['Montserrat'],
    //   display: ['Montserrat'],
    //   body: ['Montserrat']
    // },
    extend: {
      fontFamily: {
        brandLight: ['ITC Avant Garde Gothic Std'],
        brandBold: ['ITC Avant Garde Gothic LT'],
      },
      colors: {
        darkgray: '#23272F',
        lightgray: '#BFBFBF',
        cyanblue: '#82DEBE',
        aquagreen: '#94F899',
        // theme Fred
        primary: '#94F899',
        secondary: '#82DEBE',
        neutral: '#BFBFBF',
        background: '#23272F',
        backgroundAlter: '#E8E8E8',
        debug: '#bada55',
      },
      maxHeight: {
        fit: 'fit-content',
        500: '500px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
}
