import teamJosefina from '@/assets/img/josefina.png'
import teamMorella from '@/assets/img/morella.png'
import slideOne from '@/assets/img/new_slide_1.png'
import slideTwo from '@/assets/img/new_slide_2.png'
import slideThree from '@/assets/img/new_slide_3.png'
import slideFour from '@/assets/img/new_slide_4.png'
import pattern from '@/assets/img/pattern.png'
import thumbnail from '@/assets/img/thumbnail_1.jpg'
import testimonyOne from '@/assets/img/testimonio_1.png'
import bgSectionTwo from '@/assets/img/section_2.jpg'
import iconSearch from '@/assets/img/ui-icon-search.svg'
import ourTeam from '@/assets/img/our_team.png'
import glyph from '@/assets/img/ui-icon-glyph.svg'
import testimonyCompany from '@/assets/img/ui-logo-tesla.svg'
import telcelCompany from '@/assets/img/telcel.png'
import charvenecaCompany from '@/assets/img/charveneca.jpg'
export default {
  data() {
    return {
      teamJosefina,
      teamMorella,
      slideOne,
      slideTwo,
      slideThree,
      slideFour,
      pattern,
      thumbnail,
      bgSectionTwo,
      iconSearch,
      testimonyOne,
      ourTeam,
      testimonyCompany,
      glyph,
      telcelCompany,
      charvenecaCompany,
    }
  },
}
