import iconDown from '@/assets/img/ui-icon-arrow-down.png'
import iconPlus from '@/assets/img/ui-icon-plus.svg'
import iconCheck from '@/assets/img/ui-icon-check.svg'
import iconClose from '@/assets/img/ui-icon-close.svg'
import iconCloseBadge from '@/assets/img/ui-icon-close-badge.svg'
import iconError from '@/assets/img/ui-icon-error.svg'
export default {
  data() {
    return {
      iconDown,
      iconPlus,
      iconCheck,
      iconClose,
      iconCloseBadge,
      iconError,
      levelLangOptions: [
        {
          id: 1,
          name: 'Nativo',
        },
        {
          id: 2,
          name: 'Alto',
        },
        {
          id: 3,
          name: 'Intermedio',
        },
        {
          id: 4,
          name: 'Bajo',
        },
      ],
    }
  },
  methods: {
    cancel() {
      localStorage.removeItem('profilesIdentification')
      localStorage.removeItem('profilesContact')
      localStorage.removeItem('profilesIndustrySector')
      localStorage.removeItem('profilesProducts')
      localStorage.removeItem('profilesRequests')
      localStorage.removeItem('profilesLocations')
      localStorage.removeItem('profilesStudies')
      localStorage.removeItem('profilesYearsOfExperience')
      this.$router.push('/')
    },
    checkSteps(res) {
      let route
      // Add data into localStorage
      route = '/profiles/identification'
      if (Object.prototype.hasOwnProperty.call(res, 'identification')) {
        if (Object.prototype.hasOwnProperty.call(res, 'profilesContact')) {
          const profilesContact = {
            userId: res.profilesContact.userId,
            personId: res.profilesContact.personId,
            firstnames: res.profilesContact.firstnames,
            lastnames: res.profilesContact.lastnames,
            countryCode: res.profilesContact.countryCode,
            telephone: res.profilesContact.telephone,
            email: res.profilesContact.email,
          }
          localStorage.setItem(
            'profilesContact',
            JSON.stringify(profilesContact)
          )
        }
        if (
          Object.prototype.hasOwnProperty.call(res, 'profilesIndustrySector')
        ) {
          const profilesIndustrySector = {
            activities: res.profilesIndustrySector,
          }
          localStorage.setItem(
            'profilesIndustrySector',
            JSON.stringify(profilesIndustrySector)
          )
        }
        if (Object.prototype.hasOwnProperty.call(res, 'profilesProducts')) {
          const profilesProducts = {
            products: res.profilesProducts,
          }
          localStorage.setItem(
            'profilesProducts',
            JSON.stringify(profilesProducts)
          )
        }
        route = '/profiles/contact'
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesContact')) {
        route = '/profiles/industry-sector'
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesIndustrySector')) {
        route = '/profiles/products'
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesProducts')) {
        route = '/profiles/profile-requests'
      } else if (
        Object.prototype.hasOwnProperty.call(res, 'profilesRequests') &&
        !this.getLocalStorage('profilesProducts')
      ) {
        const profilesProducts = {
          products: [],
        }
        localStorage.setItem(
          'profilesProducts',
          JSON.stringify(profilesProducts)
        )
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesRequests')) {
        const profilesRequests = {
          positions: res.profilesRequests,
        }
        localStorage.setItem(
          'profilesRequests',
          JSON.stringify(profilesRequests)
        )
        route = '/profiles/profile-locations'
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesLocations')) {
        const profilesLocations = {
          positionRequestsLocations: res.profilesLocations,
        }
        localStorage.setItem(
          'profilesLocations',
          JSON.stringify(profilesLocations)
        )
        route = '/profiles/profile-studies'
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesStudies')) {
        const profilesStudies = {
          posResDegrees: res.profilesStudies,
        }
        localStorage.setItem('profilesStudies', JSON.stringify(profilesStudies))
        route = '/profiles/profile-skills'
      } else if (
        Object.prototype.hasOwnProperty.call(res, 'profilesYearsOfExperience')
      ) {
        const profilesStudies = {
          posResDegrees: [],
        }
        localStorage.setItem('profilesStudies', JSON.stringify(profilesStudies))
      }
      if (Object.prototype.hasOwnProperty.call(res, 'profilesSkills')) {
        localStorage.setItem(
          'profilesSkills',
          JSON.stringify(res.profilesSkills)
        )
        route = '/profiles/profile-years-of-experience'
      } else if (
        Object.prototype.hasOwnProperty.call(res, 'profilesYearsOfExperience')
      ) {
        const profileSkills = []
        localStorage.setItem('profileSkills', JSON.stringify(profileSkills))
      }
      if (
        Object.prototype.hasOwnProperty.call(res, 'profilesYearsOfExperience')
      ) {
        localStorage.setItem(
          'profilesYearsOfExperience',
          JSON.stringify(res.profilesYearsOfExperience)
        )
        localStorage.setItem('skillsArr', JSON.stringify(res.skillsArr))
        localStorage.setItem('languagesArr', JSON.stringify(res.languagesArr))
        localStorage.setItem(
          'languageLevelsArr',
          JSON.stringify(res.languageLevelsArr)
        )
        localStorage.setItem('countriesArr', JSON.stringify(res.countriesArr))
        localStorage.setItem('ageRangesArr', JSON.stringify(res.ageRangesArr))
        localStorage.setItem('expRangesArr', JSON.stringify(res.expRangesArr))
        localStorage.setItem(
          'companyActivitiesArr',
          JSON.stringify(res.companyActivitiesArr)
        )
        localStorage.setItem('studyTypesArr', JSON.stringify(res.studyTypesArr))
        localStorage.setItem('degreesArr', JSON.stringify(res.degreesArr))
        route = '/profiles/confirm-information'
      }
      if (
        Object.prototype.hasOwnProperty.call(res, 'error') &&
        res.error !== false
      ) {
        alert('ha ocurrido un error al cambiar de paso')
      } else {
        this.$router.push(route)
      }
    },
    getLocalStorage(step) {
      const localStep = JSON.parse(localStorage.getItem(step))
      return localStep || false
    },
  },
}
