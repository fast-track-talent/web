const initialState = () => ({
  tabs: [
    {
      id: 1,
      title: 'Diseño Organizacional',
      content:
        'Un correcto diseño organizacional parte de los objetivos empresariales, sintetiza los mismos en tareas y responsabilidades para posteriormente traducirlos en puestos detrabajo y departamentos necesarios.',
    },
    {
      id: 2,
      title: 'Cultura Organizacional',
      content:
        'Entendemos como cultura organizacional aquellas normas y valores por los que se rige una empresa, y los principios relacionados con la estructura de la compañía. En Fast Track Talent definimos una cultura organizacional para generar un impacto directo y positivo sobre la reputación externa e interna de la compañía.',
    },
    {
      id: 3,
      title: 'Indicadores de gestión',
      content:
        'Diseñamos para tu empresa los mejores indicadores de gestión o KPI para que puedas medir y evaluar resultados. Recuerda que todo lo que se hace se puede medir, por ende se puede controlar, dirigir y finalmente mejorar.',
    },
    {
      id: 4,
      title: 'Manejo de cambio organizacional',
      content:
        'La gestión del cambio consiste en aprovechar las novedades en el entorno empresarial para beneficiar a la empresa. No se trata de implementar los últimos modelos de gestión empresarial, si no de anticiparse a los cambios que hay y también, crear una estructura empresarial lo suficientemente flexible como para responder a ellos.',
    },
    {
      id: 5,
      title: 'Planificación de carrera',
      content:
        'Ser corporativamente transparente con las posibilidades de desarrollo profesional dentro de la compañía, genera en los colaboradores que buscan acelerar su desarrollo profesional, un aumento notable en la motivación. Al mismo tiempo, esta planificación, al requerir la construcción de perfiles, descripciones y aptitudes necesarias para los cargos, facilita los procesos de reemplazo, retención y captación de talento.',
    },
    {
      id: 6,
      title: 'Asesoramiento de ejecutivo',
      content:
        'Nuestro asesoramiento ejecutivo permite el correcto diagnóstico de los puntos de fortaleza y las áreas de mejora en esta área, vital para determinar planes de acción con miras a su desarrollo profesional, el diseño de su plan de carrera y aptitudes.',
    },
    {
      id: 7,
      title: 'Evaluación 360',
      content:
        'Al tratarse de una retroalimentación objetiva, compuesta por múltiples puntos de vista y perspectivas de sus empleados, departamentos relacionados, clientes externos y cargos directivos, nuestro servicio le brinda un análisis y un reporte de manera que evidencie un mapa claro y fiable de fortalezas y zonas de mejora, que en conjunto a la compañía se procede a desarrollar.',
    },
    {
      id: 8,
      title: 'Coaching de ejecutivos',
      content:
        'Tiene como objetivo principal identificar de cero o desarrollar en base a los datos del informe 360 los puntos de fortaleza y las áreas de mejora de los colaboradores de la compañía en base a la psicología empresarial.',
    },
    {
      id: 9,
      title: 'Coaching de equipos',
      content:
        'Tiene como objetivo principal identificar de cero o desarrollar en base a los datos del informe 360 los puntos de fortaleza y las áreas de mejora de los equipos que conforman la estructura organizacional de la compañía en base a la psicología empresarial.',
    },
    {
      id: 10,
      title: 'Formación de liderazgo',
      content:
        'Desarrollar habilidades interpersonales orientadas a conseguir liderazgo y proactividad en tus colaboradores yempleados, como consecuencia tendrás un equipo más eficiente, motivado y productivo.',
    },
    {
      id: 11,
      title: 'Equipos de alto desempeño',
      content:
        'Formamos a tu equipo para convertirlo en uno de alto desempeño, un equipo proactivo, motivado y con la más óptima capacidad para la toma de decisiones, de esta manera se garantizan los mejores resultados posibles.',
    },
    {
      id: 12,
      title: 'Selección de talento',
      content:
        'El proceso de selección de Fast Track Talent consiste en una serie de pasos que te garantizan la elección del aspirante que se encuentre más apto para ocupar el cargo vacante.',
    },
  ],
  selectedTab: {
    id: 1,
    title: 'Diseño Organizacional',
    content:
      'Un correcto diseño organizacional parte de los objetivos empresariales, sintetiza los mismos en tareas y responsabilidades para posteriormente traducirlos en puestos detrabajo y departamentos necesarios.',
  },
})

const state = initialState

const getters = {
  tabs(state) {
    return state.tabs
  },
  selectedTab(state) {
    return state.selectedTab
  },
}

const actions = {
  setSelectedTab({ commit }, tab) {
    commit('UPDATE_TAB', tab)
  },
}

const mutations = {
  UPDATE_TAB(state, tab) {
    state.selectedTab = tab
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}
